package PSP.U1.ACTIVIDAD1;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class CargarFicheros implements Runnable{

    public void run(){
        try {
            if (Thread.currentThread().getName().equals("prioridad1")) {
                System.out.println("Creando fichero 1");
                String ruta1 = "Ficheros/fichero1.txt";
                String nombre1 = "prioridad1";
                new CrearFicheros(ruta1, nombre1);
            }
            Thread.sleep(500);
            if (Thread.currentThread().getName().equals("prioridad2")){
                System.out.println("Creando fichero 2");
                String ruta2 = "Ficheros/fichero2.txt";
                String nombre2 = "prioridad2";
                new CrearFicheros(ruta2, nombre2);
            }
            Thread.sleep(1000);
            if (Thread.currentThread().getName().equals("prioridad3")){
                System.out.println("Creando fichero 3");
                String ruta3 = "Ficheros/fichero3.txt";
                String nombre3 = "prioridad3";
                new CrearFicheros(ruta3, nombre3);
            }
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
    }

}
